<?php

/**
 * Rotations add-on.
 * 
 * This script allows to perform a rotation around a given center.
 *
 * @author Olivier <oliver@fpdf.org?subject=Rotations>
 */
class FPDF_Rotate extends FPDF {
  var $angle = 0;

  /**
   * Performs rotation around a given center.
   *
   * The rotation affects all elements which are printed after the method call
   * (with the exception of clickable areas).
   *
   * Remarks:
   *  - Only the display is altered. The GetX() and GetY() methods are not
   *    affected, nor the automatic page break mechanism.
   *  - Rotation is not kept from page to page. Each page begins with a null
   *    rotation.
   *
   * @param float $angle The angle in degrees
   * @param float $x abscissa of the rotation center (default: current position)
   * @param float $y ordinate of the rotation center (default: current position)
   * @return void
   */
  function Rotate($angle, $x = -1, $y = -1) {
    if ($x == -1) $x = $this->x;
    if ($y == -1) $y = $this->y;
    if ($this->angle != 0) $this->_out('Q');

    $this->angle = $angle;

    if ($angle != 0) {
      $angle *= M_PI / 180;
      $c = cos($angle);
      $s = sin($angle);
      $cx = $x * $this->k;
      $cy = ($this->h - $y) * $this->k;
      $this->_out(sprintf('q %.5f %.5f %.5f %.5f %.2f %.2f cm 1 0 0 1 %.2f %.2f cm', $c, $s, - $s, $c, $cx, $cy, - $cx, - $cy));
    }
  }

  function _endpage() {
    if ($this->angle != 0) {
      $this->angle = 0;
      $this->_out('Q');
    }

    parent::_endpage();
  }
}
