
PDF Watermark
-------------
by Joakim Stai, joakimstai@gmail.com

This module adds watermarks to PDF files as they are downloaded. The watermark's
text may contain tokens, allowing for dynamic watermarks. Works with either
private files or Ubercart's File Downloads module.


--------------------------------------------------------------------------------
                                  Requirements
--------------------------------------------------------------------------------

  * PDFtk installed on the system
  * Poppler utilities installed on the system
  * FPDF library
  * Either private file downloads or Ubercart with its File Downloads module


--------------------------------------------------------------------------------
                                  Installation
--------------------------------------------------------------------------------

  1. Install PDFtk and Poppler utilities

    * Install PDFtk ("pdftk") using your system's package manager or build from
      source (see http://www.pdflabs.com/docs/install-pdftk/)

    * Install Poppler's utilities ("poppler-utils") using your system's package
      manager or build from source (see http://poppler.freedesktop.org/)

  2. Install FPDF

    * Download the latest version from http://www.fpdf.org/

    * Install the library as described on http://drupal.org/node/1440066, using
      "fpdf" as the name of the library

  3. Install jQuery miniColors (optional, but it's a really neat color picker!)

    * Download the latest version from http://git.io/NkGkAw

    * Install the library just as you did for FPDF, using "minicolors" as the
      name of the library


--------------------------------------------------------------------------------
                                Thank you notes
--------------------------------------------------------------------------------

Thank you, Wunderkraut Norway (wunderkraut.com), for contracting me to develop
this module. One more issue queue can't possibly hurt..

Thank you, Ulrich at PDF Tools (pdf-tools.com), for optimizing the preview.pdf
file!
