<?php
  
/**
 * @file
 * Administration page.
 */

/**
 * Settings form.
 */
function pdfwatermark_settings_form($form, &$form_state) {
  // Warn if FPDF has not yet been installed.
  if (!(($fpdf = libraries_detect('fpdf')) && !empty($fpdf['installed']))) {
    drupal_set_message(t('The FPDF library could not be found. Install FPDF by following the installation instructions.'), 'warning');
    return $form;
  }

  // Use miniColors colorpicker, if the library has been installed.
  if (($minicolors = libraries_load('minicolors')) && !empty($minicolors['loaded'])) {
    drupal_add_js(drupal_get_path('module', 'pdfwatermark') . '/pdfwatermark.admin.js');
  }

  // Add CSS.
  drupal_add_css(drupal_get_path('module', 'pdfwatermark') .'/pdfwatermark.admin.css');

  $form['cell'] = array(
    '#type' => 'fieldset',
    '#title' => t('Container'),
  );

  $form['cell']['values'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')), 
  );

  // Top offset.
  $form['cell']['values']['pdfwatermark_cell_top'] = array(
    '#type' => 'textfield',
    '#title' => t('Top'),
    '#field_suffix' => 'pt',
    '#default_value' => variable_get('pdfwatermark_cell_top', 225),
    '#size' => 3,
  );

  // Left offset.
  $form['cell']['values']['pdfwatermark_cell_left'] = array(
    '#type' => 'textfield',
    '#title' => t('Left'),
    '#field_suffix' => 'pt',
    '#default_value' => variable_get('pdfwatermark_cell_left', 0),
    '#size' => 3,
  );

  // Cell width.
  $form['cell']['values']['pdfwatermark_cell_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#field_suffix' => 'pt',
    '#default_value' => variable_get('pdfwatermark_cell_width', 260),
    '#size' => 3,
  );

  // Cell angle (rotation).
  $form['cell']['values']['pdfwatermark_cell_angle'] = array(
    '#type' => 'textfield',
    '#title' => t('Angle'),
    '#field_suffix' => 'degrees',
    '#default_value' => variable_get('pdfwatermark_cell_angle', 45),
    '#size' => 3,
  );

  $form['text'] = array(
    '#type' => 'fieldset',
    '#title' => t('Text'),
  );

  $form['text']['formatting'] = array(
    '#type' => 'container', 
    '#attributes' => array('class' => array('container-inline')), 
  );

  $fonts = array(
    'Arial' => t('Arial'),
    'Courier' => t('Courier'),
    'Times' => t('Times'),
  );

  // Font family.
  $form['text']['formatting']['pdfwatermark_font_family'] = array(
    '#type' => 'select',
    '#title' => t('Font'),
    '#default_value' => variable_get('pdfwatermark_font_family', 'Arial'),
    '#options' => $fonts,
  );

  $sizes = array();
  for ($i = 8; $i <= 60; $i += 2) {
    $sizes[$i] = $i;
  }

  // Font size.
  $form['text']['formatting']['pdfwatermark_font_size'] = array(
    '#type' => 'select',
    '#title' => t('Size'),
    '#field_suffix' => 'pt',
    '#default_value' => variable_get('pdfwatermark_font_size', 40),
    '#options' => $sizes,
  );

  // Font style.
  $form['text']['formatting']['pdfwatermark_font_style'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Style'),
    '#default_value' => variable_get('pdfwatermark_font_style', array('B')),
    '#options' => array(
      'B' => t('Bold'),
      'I' => t('Italic'),
      'U' => t('Underline'),
    ),
  );

  // Color.
  $form['text']['formatting']['pdfwatermark_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Color'),
    '#size' => 7,
    '#default_value' => variable_get('pdfwatermark_color', '#ECECEC'),
  );

  // Opacity (alpha).
  $form['text']['formatting']['pdfwatermark_cell_alpha'] = array(
    '#type' => 'select',
    '#title' => t('Opacity'),
    '#default_value' => variable_get('pdfwatermark_cell_alpha', 1),
    '#options' => array(
      '0.0' => t('0%'),
      '0.1' => t('10%'),
      '0.2' => t('20%'),
      '0.3' => t('30%'),
      '0.4' => t('40%'),
      '0.5' => t('50%'),
      '0.6' => t('60%'),
      '0.7' => t('70%'),
      '0.8' => t('80%'),
      '0.9' => t('90%'),
      '1.0' => t('100%'),
    ),
  );

  // Alignment.
  $form['text']['formatting']['pdfwatermark_align'] = array(
    '#type' => 'select',
    '#title' => t('Align'),
    '#default_value' => variable_get('pdfwatermark_align', 'C'),
    '#options' => array(
      'L' => t('Left'),
      'C' => t('Center'),
      'R' => t('Right'),
      'J' => t('Justify')
    ),
  );

  // Cell height (line height, as each cell is one line).
  $form['text']['formatting']['pdfwatermark_cell_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Line height'),
    '#field_suffix' => 'pt',
    '#default_value' => variable_get('pdfwatermark_cell_height', 15),
    '#size' => 3,
  );

  // The text.
  $form['text']['pdfwatermark_text'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('pdfwatermark_text', "Licensed to [current-user:default-billing-address:first_name] [current-user:default-billing-address:last_name]\n[current-date:short]\n[site:url-brief]"),
    '#rows' => 6,
  );

  $form['text']['tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Replacement patterns'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#children' => theme('token_tree', array('token_types' => array('store'))),
  );

  $form['placement'] = array(
    '#type' => 'fieldset',
    '#title' => t('Placement'),
    '#description' => t('The watermark can be placed either behind or on top of the contents of the PDF.'),
  );

  // Watermark method.
  $form['placement']['pdfwatermark_method'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('pdfwatermark_method', 'background'),
    '#options' => array(
      'background' => t('Background'),
      'stamp' => t('Foreground'),
    ),
    '#description' => t('<em>Background</em> renders a subtle watermark behind the text. It will only be visible if the original PDF is transparent, and won\'t work with most scanned PDFs. <em>Foreground</em> resembles a stamp, and is guaranteed to work with any PDF. You may preview the two methods using existing file downloads (below) to see which works best.'),
  );

  // File to preview on.
  if (module_exists('uc_file')) {
    // TODO Only return PDF files from the store..
    $form['pdfwatermark_preview_file'] = array(
      '#type' => 'textfield',
      '#title' => t('Preview with file from Ubercart store'),
      '#description' => t('Select an existing file download to preview the watermark on.'),
      '#default_value' => '',
      '#autocomplete_path' => '_autocomplete_file',
      '#maxlength' => 255,
    );
  }

  $form['actions']['#type'] = 'actions';

  $form['actions']['preview'] = array(
    '#type' => 'submit',
    '#value' => t('Preview'),
    '#submit' => array('pdfwatermark_settings_form_preview'),
  );

  // TODO Use own submit function for more control.
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('system_settings_form_submit'),
  );

  return $form;
}

/**
 * Submit handler for the preview button.
 */
function pdfwatermark_settings_form_preview($form, &$form_state) {
  global $user;

  // Use the current form values as settings for generating the watermark.
  $settings = array(
    'method' => $form_state['values']['pdfwatermark_method'],
    'text' => $form_state['values']['pdfwatermark_text'],
    'font' => array(
      'family' => $form_state['values']['pdfwatermark_font_family'],
      'size' => $form_state['values']['pdfwatermark_font_size'],
      'style' => implode(array_filter($form_state['values']['pdfwatermark_font_style'], 'is_string')),
    ),
    'color' => $form_state['values']['pdfwatermark_color'],
    'align' => $form_state['values']['pdfwatermark_align'],
    'cell' => array(
      'top' => $form_state['values']['pdfwatermark_cell_top'],
      'left' => $form_state['values']['pdfwatermark_cell_left'],
      'width' => $form_state['values']['pdfwatermark_cell_width'],
      'height' => $form_state['values']['pdfwatermark_cell_height'],
      'angle' => $form_state['values']['pdfwatermark_cell_angle'],
      'alpha' =>  $form_state['values']['pdfwatermark_cell_alpha'],
    ),
  );

  // If a file is specified, use it as the original PDF. Otherwise use the
  // example PDF provided with this module.
  if (!empty($form_state['values']['pdfwatermark_preview_file'])) {
    $original = uc_file_qualify_file($form_state['values']['pdfwatermark_preview_file']);
  }
  else {
    $original = drupal_get_path('module', 'pdfwatermark') . '/preview.pdf';
  }

  // Create a watermarked copy and send it to the browser.
  if ($copy = pdfwatermark_create_copy($original, $user->uid, $settings)) {
    $headers = file_get_content_headers($copy);
    file_transfer('temporary://' . $copy->filename, $headers);
  }
  else {
    // TODO Handle failure a little better.
    drupal_set_message(t('Computer says no.'), 'error');
  }
}
