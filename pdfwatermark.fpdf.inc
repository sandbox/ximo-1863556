<?php
/**
 * @file
 * Extends the FPDF class with add-on classes using an inheritance chain.
 *
 * Order:
 * FPDF › FPDF_Rotate › FPDF_Transparency > PDF
 */

$addons_path = drupal_get_path('module', 'pdfwatermark') . '/addons';
require_once($addons_path . '/fpdf.rotations.inc');
require_once($addons_path . '/fpdf.transparency.inc');

/**
 * One class to rule them all.
 */
class PDF extends FPDF_Transparency {}
