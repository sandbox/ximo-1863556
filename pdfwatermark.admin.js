jQuery(function ($) {
  var $color = $('#edit-pdfwatermark-color'),
      $alpha = $('#edit-pdfwatermark-cell-alpha');

  // Hide the Opacity field.
  $('.form-item-pdfwatermark-cell-alpha').hide();

  // Hide the Color field's input and attach the color picker.
  $color.hide().attr('data-opacity', $alpha.val()).miniColors({
    letterCase: 'uppercase',
    opacity: true,
    change: function (hex, rgb) {
      // Update the Opacity field on change.
      var value = $(this).miniColors('opacity').toFixed(1);
      $alpha.val(value);
    }
  });
});
